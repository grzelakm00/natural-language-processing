import os
import shutil
def refactor(raw):
    res = ''
    skip = False
    for letter in raw:
        if letter == '[':
            skip = True
        if not skip:
            res += letter
        if letter == ']':
            skip = False
    return res

for file in os.listdir("parties_after"):
    os.remove("parties_after/" + file)

files = os.listdir("parties_before")

for file in files:
    f = open('parties_before/' + file)
    raw = f.read()
    res = refactor(raw)
    f = open('parties_after/' + file[0:2] + "_new", 'a')
    f.write(res)
    f.write("\n")
files = os.listdir("parties_after")

for file in files:
    lines = open('parties_after/' + file, 'r').readlines()
    lines_set = set(lines)
    out = open('parties_after/' + file, 'w')
    for line in lines_set:
        out.write(line)
    out.close()

for file in files:
    if(file == 'ko_new'):
        name = 'konfederacja'
    if (file == 'po_new'):
        name = 'po'
    if (file == 'pi_new'):
        name = 'pis'
    os.rename("parties_after/" + file, 'parties_after/' + name)
    os.makedirs("../sejm_speeches_sorted_parties/" + name,exist_ok=True)


poslowie = os.listdir("../sejm_speeches_sorted")
partie = os.listdir("parties_after")


for partia in partie:
    lines = open('parties_after/' + partia, 'r').read().splitlines()
    lines_set = set(lines)
    for posel in poslowie:
        if posel.split('.')[0] in lines_set:
            shutil.copyfile("../sejm_speeches_sorted/" + posel,"../sejm_speeches_sorted_parties/" + partia + '/' + posel)