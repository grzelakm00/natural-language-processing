import os
import io

# export data to a dataframe
all_speeches = io.open(f"./data_frame/all_speeches.txt", mode="w", encoding="utf-8")
for kadencja in [8, 9]:
    for posiedzenie_file in os.listdir(f"./data_transformed (unpack)/kadencja_{kadencja}"):
        for dzien_file in os.listdir(f"./data_transformed (unpack)/kadencja_{kadencja}/{posiedzenie_file}"):
            for wypowiedz_file in os.listdir(f"./data_transformed (unpack)/kadencja_{kadencja}/{posiedzenie_file}/{dzien_file}"):
                file = io.open(f"./data_transformed (unpack)/kadencja_{kadencja}/{posiedzenie_file}/{dzien_file}/{wypowiedz_file}",
                               mode="r", encoding="utf-8")

                text = file.read()
                posel_begin = text.find("<h2 class=\"mowca\">Poseł ")
                posel_end = text.find(":</h2>")
                posel = text[(posel_begin + 24):posel_end]
                file.close()

                text = text[(posel_end + 6):]
                while len(text) > 0 and (text[0] == ' ' or text[0] == '…' or text[0] == ' ' or text[0] == '·'):
                    text = text[1:]
                while len(text) > 0 and (text[-1] == ' ' or text[-1] == '…' or text[-1] == ' ' or text[-1] == '·'):
                    text = text[:-1]

                wypowiedz_file = wypowiedz_file[:-4]
                [posiedzenie, dzien, wypowiedz] = wypowiedz_file.split()

                speech = str(kadencja) + '@@' + posiedzenie + '@@' + dzien + '@@' + wypowiedz + '@@' + posel + '@@' + text + '\n'
                all_speeches.write(speech)

all_speeches.close()
