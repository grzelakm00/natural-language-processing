### Language differences between political parties in Poland
The task is to study the properties of language used by political parties in Poland, its grammar,
vocabulary, and stylistic elements across different political parties within single period of time or
across time within one or two political parties. It should deal with the same or similar topic

### Prerequisities
All Jupyter notebooks can be run using tools like Pycharm or by running the Jupyter server via the console and opening a particular file using the command:

```
jupyter notebook notebook.ipynb
```
### Libraries
```
!pip install pystempel
```
### Additional files
To train the sentiment model and run the eda-sentimnent-analsis-model.ipynb file, you need to download the files:
```
- polish_sentiment_dataset.csv from  https://drive.google.com/file/d/1vXqUEBjUHGGy3vV2dA7LlvBjjZlQnl0D/view
- nkjp+wiki-forms-all-100-cbow-hs.txt.gz from http://dsmodels.nlp.ipipan.waw.pl/w2v.html and rename it to nkjp.txt.gz
```
and put them into ```data``` directory.

### Project structure

Schema of project structure: 

```
.
├── data                   # Datasets used in the project
├── eda                    # Jupyter notebooks used when analysing features of the dataset
├── models                 # Machine learning models prepared to predict the affiliation to the polish political parties based on the text or transcription of speech
├── renders                # Generated charts screenshots 
└── README.md
```


